use std::collections::{HashMap, HashSet};
use std::fmt::{Display, Formatter};
use std::ops::Index;

enum Rock {
    Cube,
    Round,
}

impl TryFrom<char> for Rock {
    type Error = String;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        Ok(match value {
            '#' => Rock::Cube,
            'O' => Rock::Round,
            _ => Err(format!("Invalid {value}"))?,
        })
    }
}

#[derive(Eq, PartialEq, Debug, Hash, Copy, Clone)]
enum Direction {
    North,
    East,
    South,
    West,
}

struct Platform {
    cube_rocks: Vec<bool>,
    round_rocks: HashSet<(usize, usize)>,
    pub height: usize,
    pub width: usize,
}

impl Platform {
    fn tilt(&mut self, direction: &Direction) {
        match direction {
            Direction::North => {
                let mut positions = vec![0; self.width];

                for y in 0..self.height {
                    for x in 0..self.width {
                        match self[(x, y)] {
                            Some(Rock::Cube) => {
                                let place = y + 1;
                                positions[x] = place;
                            }
                            Some(Rock::Round) => {
                                self.round_rocks.remove(&(x, y));
                                debug_assert!(self.round_rocks.insert((x, positions[x])));
                                positions[x] += 1;
                            }
                            _ => {}
                        }
                    }
                }
            }
            Direction::South => {
                let mut positions = vec![self.height - 1; self.width];

                for y in (0..self.height).rev() {
                    for x in 0..self.width {
                        match self[(x, y)] {
                            Some(Rock::Cube) => {
                                let place = y.saturating_sub(1);
                                positions[x] = place;
                            }
                            Some(Rock::Round) => {
                                self.round_rocks.remove(&(x, y));
                                debug_assert!(self.round_rocks.insert((x, positions[x])));
                                positions[x] = positions[x].saturating_sub(1);
                            }
                            _ => {}
                        }
                    }
                }
            }
            Direction::West => {
                let mut positions = vec![0; self.height];

                for x in 0..self.width {
                    for y in 0..self.height {
                        match self[(x, y)] {
                            Some(Rock::Cube) => {
                                let place = x + 1;
                                positions[y] = place;
                            }
                            Some(Rock::Round) => {
                                self.round_rocks.remove(&(x, y));
                                debug_assert!(self.round_rocks.insert((positions[y], y)));
                                positions[y] += 1;
                            }
                            _ => {}
                        }
                    }
                }
            }
            Direction::East => {
                let mut positions = vec![self.width - 1; self.height];

                for x in (0..self.width).rev() {
                    for y in 0..self.height {
                        match self[(x, y)] {
                            Some(Rock::Cube) => {
                                let place = x.saturating_sub(1);
                                positions[y] = place;
                            }
                            Some(Rock::Round) => {
                                self.round_rocks.remove(&(x, y));
                                debug_assert!(self.round_rocks.insert((positions[y], y)));
                                positions[y] = positions[y].saturating_sub(1);
                            }
                            _ => {}
                        }
                    }
                }
            }
        };
    }
}

impl Index<(usize, usize)> for Platform {
    type Output = Option<Rock>;

    fn index(&self, (x, y): (usize, usize)) -> &Self::Output {
        if self.round_rocks.contains(&(x, y)) {
            &Some(Rock::Round)
        } else if self.cube_rocks[y * self.width + x] {
            &Some(Rock::Cube)
        } else {
            &None
        }
    }
}

impl Display for Platform {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for y in 0..self.height {
            for x in 0..self.width {
                write!(
                    f,
                    "{}",
                    match self[(x, y)] {
                        None => '.',
                        Some(Rock::Cube) => '#',
                        Some(Rock::Round) => '0',
                    }
                )?;
            }
            writeln!(f, "")?;
        }
        Ok(())
    }
}

impl From<&str> for Platform {
    fn from(value: &str) -> Self {
        let lines: Vec<_> = value.lines().collect();
        let width = lines[0].len();
        let height = lines.len();

        let cube_rocks: Vec<_> = lines
            .iter()
            .flat_map(|line| line.chars().map(|c| c == '#'))
            .collect();

        let round_rocks: HashSet<_> = lines
            .iter()
            .enumerate()
            .flat_map(|(line_idx, line)| {
                line.chars()
                    .enumerate()
                    .filter(|(_, c)| *c == 'O')
                    .map(move |(row_idx, _)| (row_idx, line_idx))
            })
            .collect();

        Self {
            cube_rocks,
            round_rocks,
            width,
            height,
        }
    }
}

pub fn part1(input: &str) -> usize {
    let lines: Vec<_> = input.lines().collect();

    let width = lines[0].len();
    let height = lines.len();

    let mut cube_positions = vec![0; width];
    let mut sum = 0;

    let mut dst = vec![vec!['.'; width]; height];

    for (line_idx, line) in lines.iter().enumerate() {
        for (row_idx, c) in line.chars().enumerate() {
            if c != 'O' {
                dst[line_idx][row_idx] = c;
            }

            match c {
                '#' => cube_positions[row_idx] = line_idx + 1,
                'O' => {
                    sum += height - cube_positions[row_idx];
                    dst[cube_positions[row_idx]][row_idx] = 'O';
                    cube_positions[row_idx] += 1;
                }
                _ => {}
            }
        }
    }

    for line in dst {
        for c in line {
            print!("{c}");
        }
        println!();
    }

    sum
}

pub fn part2(input: &str) -> usize {
    let mut platform = Platform::from(input);

    let mut memo: HashMap<Vec<(usize, usize)>, usize> = HashMap::new();

    let cycle_max = 1_000_000_000;
    let mut iteration = 0;
    loop {
        if iteration == cycle_max {
            break;
        }

        for direction in [
            Direction::North,
            Direction::West,
            Direction::South,
            Direction::East,
        ] {
            platform.tilt(&direction);
        }

        let mut before: Vec<_> = platform.round_rocks.iter().cloned().collect();
        before.sort();

        if let Some(hit_iteration) = memo.get(&(before)) {
            // println!("hit! {iteration} {hit_iteration}");
            let loop_length = iteration - hit_iteration;

            let remaining = cycle_max - iteration;
            iteration = (cycle_max - remaining % loop_length) + 1;
        } else {
            memo.insert(before, iteration);
            iteration += 1;
        }
    }

    platform
        .round_rocks
        .iter()
        .map(|(_, y)| platform.height - y)
        .sum()
}
