use lib::{part1, part2};

fn main() {
    dbg!(part1(include_str!("../example.txt")));
    dbg!(part1(include_str!("../input.txt")));
    dbg!(part2(include_str!("../example.txt")));
    dbg!(part2(include_str!("../input.txt")));
}
