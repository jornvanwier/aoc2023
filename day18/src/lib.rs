
type Vertex = (isize, isize);

fn polygon_area(vertices: &[Vertex]) -> usize {
    let area = vertices
        .iter()
        .zip(vertices.iter().cycle().skip(vertices.len() - 1))
        .fold(0, |acc, (current, previous)| {
            acc + ((previous.0 + current.0) * (previous.1 - current.1))
        });
    (area / 2).abs() as usize
}

fn compute_area(offsets: &[Vertex]) -> usize {
    let mut vertices = Vec::with_capacity(offsets.len());

    let mut current_vertex = (0, 0);
    let mut perimeter = 0;

    vertices.push(current_vertex);
    for offset in offsets {
        current_vertex.0 = current_vertex.0 + offset.0;
        current_vertex.1 = current_vertex.1 + offset.1;
        perimeter += (offset.0.abs() + offset.1.abs()) as usize;
        vertices.push(current_vertex);
    }

    let area = polygon_area(&vertices);

    // Reverse Pick's theorem. Calculated area is A in A = i + b/2 - 1; Our answer is i + b.
    let interior = area - perimeter / 2 + 1;

    perimeter + interior
}

fn make_offset(direction: &str, amount: isize) -> (isize, isize) {
    match direction {
        "U" => (0, -amount),
        "D" => (0, amount),
        "L" => (-amount, 0),
        "R" => (amount, 0),
        _ => unreachable!(),
    }
}

pub fn part1(input: &str) -> usize {
    let data: Vec<_> = input
        .lines()
        .map(|line| {
            let [direction, amount, _color] = line.split_whitespace().collect::<Vec<_>>()[..]
            else {
                unreachable!()
            };

            let amount: isize = amount.parse().unwrap();
            let offset = make_offset(direction, amount);

            offset
        })
        .collect();

    compute_area(&data)
}

pub fn part2(input: &str) -> usize {
    let data: Vec<_> = input
        .lines()
        .map(|line| {
            let [_direction, _amount, color] = line.split_whitespace().collect::<Vec<_>>()[..]
            else {
                unreachable!()
            };

            let amount = isize::from_str_radix(&color[2..7], 16).unwrap();
            let direction =
                ["R", "D", "L", "U"][color.chars().nth(7).unwrap().to_digit(16).unwrap() as usize];

            make_offset(direction, amount)
        })
        .collect();

    compute_area(&data)
}
