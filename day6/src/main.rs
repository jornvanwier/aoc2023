fn main() {
    let input = include_str!("../input.txt");
    // let input = include_str!("../example.txt");

    dbg!(part1(input));
    dbg!(part2(input));
}

fn part1(input: &str) -> usize {
    let mut data = input
        .lines()
        .map(|line| {
            line.split_whitespace()
                .skip(1)
                .map(|v| v.parse().unwrap())
                .collect()
        })
        .collect::<Vec<Vec<usize>>>();

    let distances = data.pop().unwrap();
    let times = data.pop().unwrap();

    dbg!(&times);
    dbg!(&distances);

    times
        .iter()
        .zip(distances)
        .map(|(total_time, record)| {
            (0..total_time - 1)
                .map(|charge_time| charge_time * (total_time - charge_time) > record)
                .filter(|v| *v)
                .count()
        })
        .fold(1, |a, b| a * b)
}

fn part2(input: &str) -> usize {
    let mut data = input
        .lines()
        .map(|line| {
            line.replace(' ', "")
                .split(':')
                .skip(1)
                .next()
                .unwrap()
                .parse()
                .unwrap()
        })
        .collect::<Vec<usize>>();

    let record = data.pop().unwrap();
    let total_time = data.pop().unwrap();

    (0..total_time - 1)
        .map(|charge_time| charge_time * (total_time - charge_time) > record)
        .filter(|v| *v)
        .count()
}
