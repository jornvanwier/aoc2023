use std::collections::HashMap;
use std::ops::RangeInclusive;

#[derive(Debug)]
enum Op {
    Less,
    More,
}

#[derive(Debug)]
struct Rule<'input> {
    key: char,
    op: Op,
    value: usize,
    to: &'input str,
}

#[derive(Debug)]
struct Workflow<'input> {
    rules: Vec<Rule<'input>>,
    fallback: &'input str,
}

#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
struct ValueSet<T> {
    x: T,
    m: T,
    a: T,
    s: T,
}

impl<T> ValueSet<T> {
    fn by_key_mut(&mut self, key: char) -> &mut T {
        match key {
            'x' => &mut self.x,
            'm' => &mut self.m,
            'a' => &mut self.a,
            's' => &mut self.s,
            _ => unreachable!(),
        }
    }
    fn by_key(&self, key: char) -> &T {
        match key {
            'x' => &self.x,
            'm' => &self.m,
            'a' => &self.a,
            's' => &self.s,
            _ => unreachable!(),
        }
    }
}

type Rating = ValueSet<usize>;

type Ranges = ValueSet<RangeInclusive<usize>>;

macro_rules! rule_transition {
    ($to:expr,$workflow:ident,$workflows:ident) => {
        match $to {
            "A" => return true,
            "R" => return false,
            v => $workflow = &$workflows[v],
        };
    };
}

fn process(workflows: &HashMap<&str, Workflow>, rating: &Rating) -> bool {
    let mut workflow = &workflows["in"];

    'main: loop {
        for rule in &workflow.rules {
            let value = *rating.by_key(rule.key);

            if match rule.op {
                Op::Less => value < rule.value,
                Op::More => value > rule.value,
            } {
                rule_transition!(rule.to, workflow, workflows);
                continue 'main;
            }
        }

        // Nothing matched
        rule_transition!(workflow.fallback, workflow, workflows);
    }
}
fn parse_input<'input, TWorkflow: FromIterator<(&'input str, Workflow<'input>)>>(
    input: &'input str,
) -> (TWorkflow, Vec<Rating>) {
    let [workflows, ratings] = input.split("\n\n").collect::<Vec<_>>()[..] else {
        unreachable!()
    };

    let workflows = workflows
        .lines()
        .map(|line| {
            let (name, rest) = line.split_at(line.find('{').unwrap());

            // Reverse the iterator here so we can grab fallback as the first item
            let mut rest_iter = rest[1..rest.len() - 1].split(',').rev();

            let fallback = rest_iter.next().unwrap();

            let rules = rest_iter
                // Reverse the iterator again (undoing the previous reverse)
                .rev()
                .map(|rule| {
                    let rule_chars: Vec<_> = rule.chars().collect();

                    let key = rule_chars[0];
                    debug_assert!(matches!(key, 'x' | 'm' | 'a' | 's'));

                    let op = match rule_chars[1] {
                        '<' => Op::Less,
                        '>' => Op::More,
                        v => panic!("Invalid op {v}"),
                    };

                    let mut v = rule[2..].split(':');
                    let value: usize = v.next().unwrap().parse().unwrap();
                    let to = v.next().unwrap();

                    Rule { key, op, value, to }
                })
                .collect();

            (name, Workflow { rules, fallback })
        })
        .collect();

    // dbg!(&workflows);

    let ratings: Vec<_> = ratings
        .lines()
        .map(|line| {
            let [x, m, a, s] = line[1..line.len() - 1]
                .split(',')
                .map(|part| part[2..].parse().unwrap())
                .collect::<Vec<_>>()[..]
            else {
                unreachable!()
            };

            Rating { x, m, a, s }
        })
        .collect();
    (workflows, ratings)
}

fn match_destination(
    workflows: &HashMap<&str, Workflow>,
    destination: &str,
    ranges: Ranges,
) -> usize {
    match destination {
        "A" => ranges.x.count() * ranges.m.count() * ranges.a.count() * ranges.s.count(),
        "R" => 0,
        to => solve(workflows, to, ranges),
    }
}

fn solve(workflows: &HashMap<&str, Workflow>, name: &str, mut ranges: Ranges) -> usize {
    let workflow = &workflows[name];

    let mut total = 0;

    for rule in &workflow.rules {
        let mut pos = ranges.clone();
        apply_rule_positive(&mut pos, rule);

        total += match_destination(workflows, rule.to, pos);

        apply_rule_negative(&mut ranges, rule);
    }

    let destination = workflow.fallback;
    total += match_destination(workflows, destination, ranges);

    total
}

fn apply_rule_positive(ranges: &mut Ranges, pos_rule: &Rule) {
    let value = ranges.by_key_mut(pos_rule.key);
    match pos_rule.op {
        Op::Less => *value = *value.start()..=pos_rule.value - 1,
        Op::More => *value = pos_rule.value + 1..=*value.end(),
    };
}

fn apply_rule_negative(ranges: &mut Ranges, rule: &Rule) {
    let value = ranges.by_key_mut(rule.key);
    match rule.op {
        Op::Less => *value = rule.value..=*value.end(),
        Op::More => *value = *value.start()..=rule.value,
    };
}

pub fn part1(input: &str) -> usize {
    let (workflows, ratings) = parse_input::<HashMap<&str, Workflow>>(input);

    // dbg!(&ratings);

    ratings
        .iter()
        .filter(|&rating| process(&workflows, rating))
        // .inspect(|rating| println!("Accept {rating:?}"))
        .map(|rating| rating.x + rating.m + rating.a + rating.s)
        .sum()
}

pub fn part2(input: &str) -> usize {
    let (workflows, _) = parse_input::<HashMap<&str, Workflow<'_>>>(input);

    const MAX: usize = 4_000;

    let ranges = Ranges {
        x: 1..=MAX,
        m: 1..=MAX,
        a: 1..=MAX,
        s: 1..=MAX,
    };

    let target = "in";

    let values = solve(&workflows, target, ranges.clone());

    values
}
