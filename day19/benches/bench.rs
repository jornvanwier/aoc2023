use lib::{part1, part2};
use criterion::{black_box, criterion_group, criterion_main, Criterion};

pub fn criterion_benchmark
(c: &mut Criterion) {
    c.bench_function("part1 example", |b| {
        b.iter(|| part1(black_box(include_str!("../example.txt"))))
    });
    c.bench_function("part1 input", |b| {
        b.iter(|| part1(black_box(include_str!("../input.txt"))))
    });
    c.bench_function("part2 example", |b| {
        b.iter(|| part2(black_box(include_str!("../example.txt"))))
    });
    c.bench_function("part2 input", |b| {
        b.iter(|| part2(black_box(include_str!("../input.txt"))))
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
