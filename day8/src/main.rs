use std::collections::HashMap;
use std::str::Lines;

enum Direction {
    Left,
    Right,
}

fn main() {
    dbg!(part1(include_str!("../example1.txt")));
    dbg!(part1(include_str!("../input.txt")));
    dbg!(part2(include_str!("../example2.txt")));
    dbg!(part2(include_str!("../input.txt")));
}

fn count_path<F: Fn(&str) -> bool>(
    instructions: &[Direction],
    map: &HashMap<&str, (&str, &str)>,
    start: &str,
    end_condition: F,
) -> usize {
    let mut instructions = instructions.iter().cycle();

    let mut steps = 0;
    let mut node = start;

    while !end_condition(node) {
        let (left_path, right_path) = map[node];

        node = match instructions.next().unwrap() {
            Direction::Left => left_path,
            Direction::Right => right_path,
        };

        steps += 1;
    }

    steps
}

fn parse_input<'input>(
    lines: &'input mut Lines,
) -> (
    Vec<Direction>,
    HashMap<&'input str, (&'input str, &'input str)>,
) {
    let instructions: Vec<_> = lines
        .next()
        .unwrap()
        .chars()
        .map(|c| match c {
            'L' => Direction::Left,
            'R' => Direction::Right,
            _ => unreachable!(),
        })
        .collect();

    let map: HashMap<_, _> = lines
        .skip(1)
        .map(|line| {
            let [name, paths] = line.split('=').collect::<Vec<_>>()[..] else {
                unreachable!()
            };

            let [left, right] = paths
                .trim_matches(&[' ', '(', ')'][..])
                .split(", ")
                .collect::<Vec<_>>()[..]
            else {
                unreachable!()
            };

            (name.trim(), (left, right))
        })
        .collect();
    (instructions, map)
}

fn gcd(a: usize, b: usize) -> usize {
    match ((a, b), (a & 1, b & 1)) {
        ((x, y), _) if x == y => y,
        ((0, x), _) | ((x, 0), _) => x,
        ((x, y), (0, 1)) | ((y, x), (1, 0)) => gcd(x >> 1, y),
        ((x, y), (0, 0)) => gcd(x >> 1, y >> 1) << 1,
        ((x, y), (1, 1)) => {
            let (x, y) = (x.min(y), x.max(y));
            gcd((y - x) >> 1, x)
        }
        _ => unreachable!(),
    }
}

fn lcm(a: usize, b: usize) -> usize {
    a * b / gcd(a, b)
}
fn part1(input: &str) -> usize {
    let mut lines = input.lines();
    let (instructions, map) = parse_input(&mut lines);
    count_path(&instructions, &map, "AAA", |node| node == "ZZZ")
}

fn part2(input: &str) -> usize {
    let mut lines = input.lines();
    let (instructions, map) = parse_input(&mut lines);

    map.keys()
        .filter(|node| node.ends_with('A'))
        .map(|starting_node| {
            count_path(&instructions, &map, starting_node, |node| {
                node.ends_with('Z')
            })
        })
        .fold(1, |a, b| lcm(a, b))
}
