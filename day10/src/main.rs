use std::collections::HashSet;

const DIRECTIONS: [(Direction, (isize, isize)); 4] = [
    (Direction::North, (-1, 0)),
    (Direction::East, (0, 1)),
    (Direction::South, (1, 0)),
    (Direction::West, (0, -1)),
];

#[derive(Eq, PartialEq, Copy, Clone, Debug)]
enum Direction {
    North,
    East,
    South,
    West,
}

impl Direction {
    fn opposite(&self) -> Self {
        match self {
            Direction::North => Direction::South,
            Direction::East => Direction::West,
            Direction::South => Direction::North,
            Direction::West => Direction::East,
        }
    }
}

#[derive(Eq, PartialEq, Copy, Clone, Debug)]
enum Tile {
    Vertical,
    Horizontal,
    BendNE,
    BendNW,
    BendSW,
    BendSE,

    Start,
    Ground,
}

impl Tile {
    fn get_connections(&self) -> [bool; 4] {
        match self {
            Tile::Vertical => [true, false, true, false],
            Tile::Horizontal => [false, true, false, true],
            Tile::BendNE => [true, true, false, false],
            Tile::BendNW => [true, false, false, true],
            Tile::BendSW => [false, false, true, true],
            Tile::BendSE => [false, true, true, false],
            Tile::Start => [true, true, true, true],
            Tile::Ground => [false, false, false, false],
        }
    }
}

impl TryFrom<char> for Tile {
    type Error = String;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        use Tile::*;

        Ok(match value {
            '|' => Vertical,
            '-' => Horizontal,
            'L' => BendNE,
            'J' => BendNW,
            '7' => BendSW,
            'F' => BendSE,
            '.' => Ground,
            'S' => Start,
            v => Err(format!("Invalid {v}"))?,
        })
    }
}

fn find_start(grid: &Vec<Vec<Tile>>) -> (usize, usize) {
    for (y, row) in grid.iter().enumerate() {
        for (x, tile) in row.iter().enumerate() {
            if tile == &Tile::Start {
                return (y, x);
            }
        }
    }

    panic!("No start!")
}

fn is_connected(current: &Tile, next: &Tile, direction: Direction) -> bool {
    let direction_idx = match direction {
        Direction::North => 0,
        Direction::East => 1,
        Direction::South => 2,
        Direction::West => 3,
    };

    current.get_connections()[direction_idx] && next.get_connections()[(direction_idx + 2) % 4]
}

fn count_path2(grid: &Vec<Vec<Tile>>, (start_y, start_x): (usize, usize)) -> Vec<(usize, usize)> {
    let width = grid[0].len();

    for (direction, (offset_y, offset_x)) in DIRECTIONS {
        let next_y = start_y as isize + offset_y;
        let next_x = start_x as isize + offset_x;

        if !(0..grid.len() as isize).contains(&next_y) || !(0..width as isize).contains(&next_x) {
            continue;
        }

        let next_y = next_y as usize;
        let next_x = next_x as usize;

        if is_connected(&grid[start_y][start_x], &grid[next_y][next_x], direction) {
            if let Some(path) = inner(grid, (next_y, next_x), direction) {
                return path;
            }
        }
    }

    panic!("No paths");
}

fn inner(
    grid: &Vec<Vec<Tile>>,
    (start_y, start_x): (usize, usize),
    start_direction: Direction,
) -> Option<Vec<(usize, usize)>> {
    let mut path = vec![(start_y, start_x)];
    let width = grid[0].len();

    let (mut y, mut x) = (start_y, start_x);
    let mut previous_direction = start_direction;

    'outer: loop {
        if grid[y][x] == Tile::Start {
            return Some(path);
        }

        for (direction, (offset_y, offset_x)) in DIRECTIONS {
            if direction == previous_direction.opposite() {
                continue;
            }

            let next_y = y as isize + offset_y;
            let next_x = x as isize + offset_x;

            if !(0..grid.len() as isize).contains(&next_y) || !(0..width as isize).contains(&next_x)
            {
                continue;
            }

            let next_y = next_y as usize;
            let next_x = next_x as usize;

            if is_connected(&grid[y][x], &grid[next_y][next_x], direction) {
                (y, x) = (next_y, next_x);
                path.push((y, x));
                previous_direction = direction;
                continue 'outer;
            }
        }

        return None;
    }
}

fn part1(input: &str) {
    let grid: Vec<_> = input
        .lines()
        .map(|line| {
            line.chars()
                .map(|c| Tile::try_from(c))
                .collect::<Result<Vec<_>, String>>()
                .unwrap()
        })
        .collect();

    let (start_y, start_x) = find_start(&grid);

    let path = count_path2(&grid, (start_y, start_x));

    println!("part1={}", path.len() / 2);

    let mut a = vec![vec![' '; grid[0].len()]; grid.len()];

    for (y, x) in &path {
        a[*y][*x] = match grid[*y][*x] {
            Tile::Vertical => '|',
            Tile::Horizontal => '-',
            Tile::BendNE => 'L',
            Tile::BendNW => 'J',
            Tile::BendSW => '7',
            Tile::BendSE => 'F',
            Tile::Start => 'S',
            Tile::Ground => '.',
        };
    }

    let path: HashSet<(usize, usize)> = HashSet::from_iter(path);

    let mut n = 0;

    for y in 0..grid.len() {
        for x in 0..grid[0].len() {
            if path.contains(&(y, x)) {
                continue;
            }
            let value = (0..x)
                .filter(|x_check| {
                    path.contains(&(y, *x_check))
                        && matches!(
                            grid[y][*x_check],
                            Tile::Vertical | Tile::BendNE | Tile::BendNW
                        )
                })
                .count()
                % 2;
            n += value;

            if value == 1 {
                a[y][x] = 'I';
            } else {
                a[y][x] = 'O';
            }
        }
    }

    for row in &a {
        for c in row {
            print!("{c}");
        }
        println!();
    }

    println!("part2={n}");
}

fn main() {
    let input = include_str!("../input.txt");
    // let input = include_str!("../example.txt");

    part1(input);
}
