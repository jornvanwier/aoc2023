use std::collections::{HashMap, HashSet};
use std::iter;

#[derive(Eq, PartialEq, Debug, Copy, Clone, Hash)]
pub struct Position {
    x: usize,
    y: usize,
}

fn check_coord(value: usize, offset: isize, max: usize) -> Option<usize> {
    if let Some(new_value) = value.checked_add_signed(offset) {
        if new_value < max {
            return Some(new_value);
        }
    }

    None
}

type Path = HashSet<Position>;

fn find_longest_path(
    grid: &Vec<Vec<char>>,
    start: Position,
    goal: Position,
    initial_path: Path,
    cost: &mut HashMap<(Position, (isize, isize)), usize>,
    avoid_slopes: bool,
) -> Option<usize> {
    let width = grid[0].len();
    let height = grid.len();

    let mut path = initial_path;

    let mut current = start;

    loop {
        if current == goal {
            break Some(path.len());
        }

        let neighbor_offsets = [(1, 0), (-1, 0), (0, 1), (0, -1)];

        let neighbor_offsets = if avoid_slopes {
            match grid[current.y][current.x] {
                '>' => &neighbor_offsets[0..1],
                '<' => &neighbor_offsets[1..2],
                'v' => &neighbor_offsets[2..3],
                '^' => &neighbor_offsets[3..4],
                _ => &neighbor_offsets[..],
            }
        } else {
            &neighbor_offsets[..]
        };

        debug_assert!(matches!(neighbor_offsets.len(), 1 | 4));

        let valid_neighbors: Vec<_> = neighbor_offsets
            .into_iter()
            .filter_map(|offset| {
                match (
                    check_coord(current.x, offset.0, width),
                    check_coord(current.y, offset.1, height),
                ) {
                    (Some(neighbor_x), Some(neighbor_y)) => Some(Position {
                        x: neighbor_x,
                        y: neighbor_y,
                    }),
                    _ => None,
                }
            })
            .filter(|neighbor| grid[neighbor.y][neighbor.x] != '#')
            .filter(|neighbor| !path.contains(neighbor))
            .collect();

        match valid_neighbors.len() {
            1 => {
                let neighbor = valid_neighbors[0];
                path.insert(neighbor);
                current = neighbor;
            }
            2.. => {
                return valid_neighbors
                    .into_iter()
                    .map(|neighbor| {
                        let mut path = path.clone();
                        path.insert(neighbor);
                        find_longest_path(&grid, neighbor, goal, path, cost, avoid_slopes)
                            .unwrap_or(0)
                    })
                    .max();
            }
            _ => break None,
        }
    }
}

fn parse_grid(input: &str) -> Vec<Vec<char>> {
    let grid: Vec<Vec<_>> = input.lines().map(|line| line.chars().collect()).collect();
    grid
}

fn find_path_on_row(grid: &Vec<Vec<char>>, row: usize) -> Position {
    Position {
        x: grid[row].iter().position(|&c| c == '.').unwrap(),
        y: row,
    }
}

pub fn part1(input: &str) -> usize {
    let grid = parse_grid(input);

    let start = find_path_on_row(&grid, 0);
    let end = find_path_on_row(&grid, grid.len() - 1);

    debug_assert_eq!(grid[start.y][start.x], '.');
    debug_assert_eq!(grid[end.y][end.x], '.');

    find_longest_path(&grid, start, end, Path::new(), &mut HashMap::new(), true).expect("Find path")
}

pub fn longest_path_tsp(grid: &[Vec<char>], start: Position, goal: Position) -> usize {
    let nodes: HashSet<_> = grid
        .iter()
        .enumerate()
        .flat_map(|(y, row)| {
            row.iter()
                .enumerate()
                .map(move |(x, _)| Position { x, y })
                .filter(|&pos| grid[pos.y][pos.x] != '#')
                .filter(|&pos| get_neighbors(grid, pos).count() > 2)
        })
        .chain(iter::once(start))
        .chain(iter::once(goal))
        .collect();

    for &Position { x, y } in &nodes {
        debug_assert_ne!(grid[y][x], '#', "{x} {y}")
    }

    let mut connections: HashMap<_, _> = HashMap::new();

    for ((from, to), distance) in nodes.iter().flat_map(|node| {
        get_neighbors(grid, *node).filter_map(|start_neighbor| {
            let mut previous = *node;
            let mut current = start_neighbor;
            let mut path_length = 1;

            if let Some(to) = loop {
                if nodes.contains(&current) {
                    debug_assert!(
                        current == start
                            || current == goal
                            || get_neighbors(grid, current).count() > 2
                    );
                    break Some(current);
                }

                let mut neighbor_iter =
                    get_neighbors(grid, current).filter(|&neighbor| neighbor != previous);

                if let Some(next) = neighbor_iter.next() {
                    path_length += 1;
                    previous = current;
                    current = next;
                } else {
                    // Dead end
                    break None;
                }
            } {
                Some(((*node, to), path_length))
            } else {
                None
            }
        })
    }) {
        connections
            .entry(from)
            .or_insert(vec![])
            .push((to, distance))
    }

    for (&from, tos) in &connections {
        for &(to, distance) in tos {
            debug_assert!(connections[&to].contains(&(from, distance)));
        }
    }

    // dbg!(&connections);

    brute_force_tsp(start, goal, Vec::from([start]), &connections).unwrap()
}

fn brute_force_tsp(
    start: Position,
    goal: Position,
    path: Vec<Position>,
    connections: &HashMap<Position, Vec<(Position, usize)>>,
) -> Option<usize> {
    connections[&start]
        .iter()
        .filter_map(|&(to, distance)| {
            if path.contains(&(to)) {
                return None;
            }

            if to == goal {
                let path_length = path
                    .iter()
                    .zip(path.iter().skip(1))
                    .map(|(node, next)| {
                        connections[node].iter().find(|(c, _)| c == next).unwrap().1
                    })
                    .sum::<usize>()
                    + distance;
                return Some(path_length);
            }

            let mut path = path.clone();
            path.push(to);

            brute_force_tsp(to, goal, path, connections)
        })
        .max()
}

fn get_neighbors(grid: &[Vec<char>], node: Position) -> impl Iterator<Item = Position> + '_ {
    let height = grid.len();
    let width = grid[0].len();

    [(1, 0), (-1, 0), (0, 1), (0, -1)]
        .into_iter()
        .filter_map(move |offset| {
            match (
                check_coord(node.x, offset.0, width),
                check_coord(node.y, offset.1, height),
            ) {
                (Some(neighbor_x), Some(neighbor_y)) => Some(Position {
                    x: neighbor_x,
                    y: neighbor_y,
                }),
                _ => None,
            }
        })
        .filter(|value| grid[value.y][value.x] != '#')
}

pub fn part2(input: &str) -> usize {
    let grid = parse_grid(input);

    let start = find_path_on_row(&grid, 0);
    let end = find_path_on_row(&grid, grid.len() - 1);

    debug_assert_eq!(grid[start.y][start.x], '.');
    debug_assert_eq!(grid[end.y][end.x], '.');

    longest_path_tsp(&grid, start, end)
}
