use indicatif::{MultiProgress, ParallelProgressIterator, ProgressBar, ProgressIterator};
use rayon::prelude::{IntoParallelIterator, IntoParallelRefIterator, ParallelIterator};

fn main() {
    // let input = include_str!("../input.txt");
    let input = include_str!("../example.txt");

    dbg!(part1(input));
    dbg!(part2(input));
}
fn part1(input: &str) -> usize {}
    0
}

fn part2(input: &str) -> usize {
    0
}

fn part1(input: &str) -> usize {
    let mut sections = input.split("\n\n");

    let seeds_str = sections.next().unwrap();
    let seeds: Vec<usize> = seeds_str
        .split(':')
        .last()
        .unwrap()
        .split_whitespace()
        .map(|v| v.parse().unwrap())
        .collect();

    dbg!(&seeds);

    let mappings: Vec<Vec<_>> = sections
        .map(|section| {
            let mut mapping: Vec<_> = section
                .split('\n')
                .skip(1)
                .map(|line| {
                    let [dest_start, src_start, length] = line
                        .split_whitespace()
                        .map(|v| v.parse().unwrap())
                        .collect::<Vec<usize>>()[..]
                    else {
                        unreachable!()
                    };

                    (src_start..src_start + length, dest_start)
                })
                .collect();

            mapping.sort_by(|(a_range, _), (b_range, _)| a_range.start.cmp(&b_range.start));
            mapping
        })
        .collect();

    let mut lowest_location = usize::MAX;

    for seed in seeds {
        dbg!(seed);
        let mut current = seed;

        'stage: for stage in mappings.iter() {
            dbg!(current);
            for (src_range, dest_start) in stage {
                if src_range.contains(&current) {
                    current = dest_start + (current - src_range.start);
                    continue 'stage;
                } else if src_range.start > current {
                    // Mapped 1-to-1
                    continue 'stage;
                }
            }
        }

        if current < lowest_location {
            lowest_location = current;
        }
    }

    lowest_location
}

fn part2(input: &str) -> usize {
    let mut sections = input.split("\n\n");

    let seeds_str = sections.next().unwrap();
    let seed_values: Vec<usize> = seeds_str
        .split(':')
        .last()
        .unwrap()
        .split_whitespace()
        .map(|v| v.parse().unwrap())
        .collect();

    let seed_ranges: Vec<_> = seed_values
        .iter()
        .step_by(2)
        .zip(seed_values.iter().skip(1).step_by(2))
        .map(|(start, length)| *start..*start + *length)
        .collect();

    dbg!(&seed_values);

    let mappings: Vec<Vec<_>> = sections
        .map(|section| {
            let mut mapping: Vec<_> = section
                .split('\n')
                .skip(1)
                .map(|line| {
                    let [dest_start, src_start, length] = line
                        .split_whitespace()
                        .map(|v| v.parse().unwrap())
                        .collect::<Vec<usize>>()[..]
                    else {
                        unreachable!()
                    };

                    (src_start..src_start + length, dest_start)
                })
                .collect();

            mapping.sort_by(|(a_range, _), (b_range, _)| a_range.start.cmp(&b_range.start));
            mapping
        })
        .collect();

    let m = MultiProgress::new();
    let ranges_progress = m.add(ProgressBar::new(seed_ranges.len() as u64));

    seed_ranges
        .into_iter()
        .map(|seed_range| {
            let seed_progress =
                m.insert_after(&ranges_progress, ProgressBar::new(seed_range.len() as u64));
            let min_location = seed_range
                .into_par_iter()
                .map(|seed| {
                    let mut current = seed;

                    'stage: for stage in mappings.iter() {
                        // dbg!(current);
                        for (src_range, dest_start) in stage {
                            if src_range.contains(&current) {
                                current = dest_start + (current - src_range.start);
                                continue 'stage;
                            } else if src_range.start > current {
                                // Mapped 1-to-1
                                continue 'stage;
                            }
                        }
                    }

                    current
                })
                .progress_with(seed_progress.clone())
                .min()
                .unwrap();

            m.remove(&seed_progress);

            min_location
        })
        .progress_with(ranges_progress.clone())
        .min()
        .unwrap()
}

fn part2_slow(input: &str) -> usize {
    let mut sections = input.split("\n\n");

    let seeds_str = sections.next().unwrap();
    let seed_values: Vec<usize> = seeds_str
        .split(':')
        .last()
        .unwrap()
        .split_whitespace()
        .map(|v| v.parse().unwrap())
        .collect();

    let seed_ranges: Vec<_> = seed_values
        .iter()
        .step_by(2)
        .zip(seed_values.iter().skip(1).step_by(2))
        .map(|(start, length)| *start..*start + *length)
        .collect();

    dbg!(&seed_values);

    let mappings: Vec<Vec<_>> = sections
        .map(|section| {
            let mut mapping: Vec<_> = section
                .split('\n')
                .skip(1)
                .map(|line| {
                    let [dest_start, src_start, length] = line
                        .split_whitespace()
                        .map(|v| v.parse().unwrap())
                        .collect::<Vec<usize>>()[..]
                    else {
                        unreachable!()
                    };

                    (src_start..src_start + length, dest_start)
                })
                .collect();

            mapping.sort_by(|(a_range, _), (b_range, _)| a_range.start.cmp(&b_range.start));
            mapping
        })
        .collect();

    let mut lowest_location = usize::MAX;

    let m = MultiProgress::new();
    let ranges_progress = m.add(ProgressBar::new(seed_ranges.len() as u64));

    for seed_range in seed_ranges {
        ranges_progress.inc(1);
        let seed_progress =
            m.insert_after(&ranges_progress, ProgressBar::new(seed_range.len() as u64));
        for seed in seed_range {
            seed_progress.inc(1);
            // dbg!(seed);
            let mut current = seed;

            'stage: for stage in mappings.iter() {
                // dbg!(current);
                for (src_range, dest_start) in stage {
                    if src_range.contains(&current) {
                        current = dest_start + (current - src_range.start);
                        continue 'stage;
                    } else if src_range.start > current {
                        // Mapped 1-to-1
                        continue 'stage;
                    }
                }
            }

            if current < lowest_location {
                lowest_location = current;
            }
        }

        m.remove(&seed_progress);
    }

    lowest_location
}
