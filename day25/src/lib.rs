use std::collections::{HashMap, HashSet};

pub fn part1(input: &str) -> usize {
    let mut connections: HashMap<_, HashSet<_>> = input
        .lines()
        .map(|line| {
            let (from, tos) = line.split_at(3);

            (from, tos.split_whitespace()
                .skip(1)
                .inspect(|&to| debug_assert!(to.len() == 3, "{to}"))
                .collect())
        })
        .collect();

    let mut reverse = vec![];
    for (&from, tos) in &connections {
        for &to in tos {
            reverse.push((to, from));

        }
    }

    for (from, to) in reverse{
        connections.entry(from).or_insert(HashSet::new()).insert(to);
    }

    let mut left: HashSet<&str> = HashSet::from_iter(connections.keys().cloned());

    fn count(node: &str, left: &HashSet<&str>, connections: &HashMap<&str,HashSet<&str>>) -> usize { (&connections[node] - left).len() }
    while left.iter().map(|node| count(node, &left, &connections)).sum::<usize>() != 3 {
        let &max = left.iter().max_by_key(|&node| count(node, &left, &connections)).unwrap();
        left.remove(&max);
    }

    left.len() * (connections.len() - left.len())
}

pub fn part2(_input: &str) -> usize {
    0
}
