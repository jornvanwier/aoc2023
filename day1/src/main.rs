fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../input.txt");

    dbg!(part1(input));
    dbg!(part2(input));
}

fn part1(input: &str) -> u32 {
    input.lines().map(|line| {
        let mut first = None;
        let mut last = None;
        for character in line.chars() {
            if let Some(current) = character.to_digit(10) {
                last = Some(current);
                if first.is_none() {
                    first = Some(current);
                }
            }
        }

        first.unwrap_or(0) * 10 + last.unwrap_or(0)
    }).sum()
}

fn part2(input: &str) -> u32 {
    input.lines().map(|line| {
        let mut first = None;
        let mut last = None;

        let mut idx = 0;

        while idx < line.len() {
            let value = match line[idx..].chars().take(5).collect::<Vec<_>>()[..] {
                ['o', 'n', 'e', ..] => Some(1),
                ['t', 'w', 'o', ..] => Some(2),
                ['t', 'h', 'r', 'e', 'e', ..] => Some(3),
                ['f', 'o', 'u', 'r', ..] => Some(4),
                ['f', 'i', 'v', 'e', ..] => Some(5),
                ['s', 'i', 'x', ..] => Some(6),
                ['s', 'e', 'v', 'e', 'n', ..] => Some(7),
                ['e', 'i', 'g', 'h', 't', ..] => Some(8),
                ['n', 'i', 'n', 'e', ..] => Some(9),
                [d, ..] => d.to_digit(10),
                [] => unreachable!(),
            };

            if value.is_some() {
                if first.is_none() {
                    first = value;
                }
                last = value;
            }

            idx += 1;
        }

        first.unwrap_or(0) * 10 + last.unwrap_or(0)
    }).sum()
}
