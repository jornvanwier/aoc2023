use regex::Regex;

fn main() {
    let input = include_str!("../input.txt");
    let input = include_str!("../example.txt");

    dbg!(part1(input));
    dbg!(part2(input));
}

fn part1(input: &str) -> usize {
    let reds_available = 12;
    let greens_available = 13;
    let blues_available = 14;

    input
        .lines()
        .map(|line| {
            let matches = Regex::new(r"Game (\d+):(.+)")
                .unwrap()
                .captures(line)
                .expect("Invalid line");
            let id = &matches[1];
            let remainder = &matches[2];

            let hand_regex = Regex::new(r"(\d+) (red|green|blue)").unwrap();
            let possible = remainder.split(";").all(|grab| {
                let mut grab_red = 0;
                let mut grab_green = 0;
                let mut grab_blue = 0;

                for hand in hand_regex.captures_iter(grab) {
                    let amount = &hand[1].parse().unwrap();
                    let color = &hand[2];

                    match color {
                        "red" => grab_red += amount,
                        "green" => grab_green += amount,
                        "blue" => grab_blue += amount,
                        _ => unreachable!(),
                    }
                }

                if grab_red <= reds_available
                    && grab_green <= greens_available
                    && grab_blue <= blues_available
                {
                    true
                } else {
                    false
                }
            });

            if possible {
                id.parse().unwrap()
            } else {
                0
            }
        })
        .sum()
}

fn part2(input: &str) -> usize {
    input
        .lines()
        .map(|line| {
            let matches = Regex::new(r"Game (\d+):(.+)")
                .unwrap()
                .captures(line)
                .expect("Invalid line");
            let remainder = &matches[2];

            let mut reds_available = 0;
            let mut greens_available = 0;
            let mut blues_available = 0;

            let hand_regex = Regex::new(r"(\d+) (red|green|blue)").unwrap();
            for grab in remainder.split(";") {
                let mut grab_red = 0;
                let mut grab_green = 0;
                let mut grab_blue = 0;

                for hand in hand_regex.captures_iter(grab) {
                    let amount = &hand[1].parse().unwrap();
                    let color = &hand[2];

                    match color {
                        "red" => grab_red += amount,
                        "green" => grab_green += amount,
                        "blue" => grab_blue += amount,
                        _ => unreachable!(),
                    }
                }

                reds_available = reds_available.max(grab_red);
                greens_available = greens_available.max(grab_green);
                blues_available = blues_available.max(grab_blue);
            }

            reds_available * greens_available * blues_available
        })
        .sum()
}
