use itertools::Itertools;
use std::ops::Sub;
use z3::ast::Ast;
use z3::SatResult;

type Value = f64;

#[derive(PartialEq, Debug, Clone)]
struct Vec3 {
    x: Value,
    y: Value,
    z: Value,
}

impl From<&str> for Vec3 {
    fn from(value: &str) -> Self {
        let [x, y, z] = value
            .split(',')
            .map(str::trim)
            .map(str::parse)
            .map(Result::unwrap)
            .collect_vec()[..]
        else {
            unreachable!()
        };
        Self { x, y, z }
    }
}

impl Sub for &Vec3 {
    type Output = Vec3;

    fn sub(self, rhs: Self) -> Self::Output {
        Vec3 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

#[derive(PartialEq, Debug, Clone)]
struct Particle {
    position: Vec3,
    velocity: Vec3,
}

fn parse_input(input: &str) -> impl Iterator<Item = Particle> + '_ {
    input.lines().map(|line| {
        let mut parts = line.split('@').map(Vec3::from);
        let position = parts.next().unwrap();
        let velocity = parts.next().unwrap();
        Particle { position, velocity }
    })
}

pub fn part1(input: &str, test_area: (Value, Value)) -> usize {
    let data = parse_input(input).collect_vec();

    let lines = data
        .iter()
        .map(|Particle { position, velocity }| {
            // Look a few ns ahead. Theoretically we only care about the difference between the points,
            // but in practice float precision gets in the way.
            Vec3 {
                x: position.x + velocity.x * 100.,
                y: position.y + velocity.y * 100.,
                z: position.z + velocity.z * 100.,
            }
        })
        .zip(&data)
        .map(
            |(
                end_point,
                Particle {
                    position: start_point,
                    ..
                },
            )| (start_point.clone(), end_point),
        )
        .collect_vec();

    let mut count = 0;
    for (idx, line_a) in lines.iter().enumerate() {
        for line_b in &lines[idx + 1..] {
            debug_assert_ne!(line_a, line_b);

            if let Some(intersection) = lines_intersect(line_a, line_b, test_area) {
                let in_future_a = intersection_is_in_future(line_a, &intersection);
                let in_future_b = intersection_is_in_future(line_b, &intersection);
                let intersect_in_future = in_future_a && in_future_b;

                if intersect_in_future {
                    // Intersection is in future
                    count += 1;

                    // println!("{line_a:?}\n{line_b:?}\n");
                }
            }
        }
    }

    count
}

fn intersection_is_in_future(line: &(Vec3, Vec3), intersection: &Vec3) -> bool {
    (intersection.x - line.0.x).signum() == (line.1.x - line.0.x).signum()
        && (intersection.y - line.0.y).signum() == (line.1.y - line.0.y).signum()
}

#[allow(unused_variables)]
fn lines_intersect(
    (Vec3 { x: x1, y: y1, .. }, Vec3 { x: x2, y: y2, .. }): &(Vec3, Vec3),
    (Vec3 { x: x3, y: y3, .. }, Vec3 { x: x4, y: y4, .. }): &(Vec3, Vec3),
    (test_area_min, test_area_max): (Value, Value),
) -> Option<Vec3> {
    let denominator = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);

    if denominator == 0. {
        // Parallel
        return None;
    }

    let p_x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / denominator;
    let p_y = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / denominator;

    let test_area = test_area_min..=test_area_max;
    if test_area.contains(&p_x) && test_area.contains(&p_y) {
        Some(Vec3 {
            x: p_x,
            y: p_y,
            z: 0.,
        })
    } else {
        None
    }
}

pub fn part2(input: &str) -> usize {
    let hail = parse_input(input);

    let config = z3::Config::new();
    let ctx = z3::Context::new(&config);

    let rock_pos_x = z3::ast::Int::new_const(&ctx, "px");
    let rock_pos_y = z3::ast::Int::new_const(&ctx, "py");
    let rock_pos_z = z3::ast::Int::new_const(&ctx, "pz");

    let rock_vel_x = z3::ast::Int::new_const(&ctx, "vx");
    let rock_vel_y = z3::ast::Int::new_const(&ctx, "vy");
    let rock_vel_z = z3::ast::Int::new_const(&ctx, "vz");

    let zero = z3::ast::Int::from_i64(&ctx, 0);

    let solver = z3::Solver::new(&ctx);

    for (idx, Particle { position, velocity }) in hail.enumerate() {
        let pos_x = z3::ast::Int::from_i64(&ctx, position.x as i64);
        let pos_y = z3::ast::Int::from_i64(&ctx, position.y as i64);
        let pos_z = z3::ast::Int::from_i64(&ctx, position.z as i64);

        let vel_x = z3::ast::Int::from_i64(&ctx, velocity.x as i64);
        let vel_y = z3::ast::Int::from_i64(&ctx, velocity.y as i64);
        let vel_z = z3::ast::Int::from_i64(&ctx, velocity.z as i64);

        let t = z3::ast::Int::new_const(&ctx, format!("t{idx}"));

        solver.assert(&(t.ge(&zero)));
        solver.assert(&(pos_x + &t * vel_x)._eq(&(&rock_pos_x + &t * &rock_vel_x)));
        solver.assert(&(pos_y + &t * vel_y)._eq(&(&rock_pos_y + &t * &rock_vel_y)));
        solver.assert(&(pos_z + &t * vel_z)._eq(&(&rock_pos_z + &t * &rock_vel_z)));
    }

    debug_assert_eq!(solver.check(), SatResult::Sat);
    let model = solver.get_model().unwrap();
    let x = model.eval(&rock_pos_x, true).unwrap().as_i64().unwrap();
    let y = model.eval(&rock_pos_y, true).unwrap().as_i64().unwrap();
    let z = model.eval(&rock_pos_z, true).unwrap().as_i64().unwrap();

    (x + y + z) as usize
}

#[cfg(test)]
mod tests {
    use crate::part1;

    #[test]
    fn test_part1_example() {
        let value = part1(include_str!("../example.txt"), (7., 27.));
        assert_eq!(value, 2)
    }

    #[test]
    fn test_part1_input() {
        let value = part1(
            include_str!("../input.txt"),
            (200000000000000., 400000000000000.),
        );
        assert_eq!(value, 20336)
    }
}
