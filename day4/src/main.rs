use std::collections::HashSet;
use std::fs::File;
use std::io::Write;
use std::ptr::write;

fn main() {
    let input = include_str!("../input.txt");
    // let input = include_str!("../example.txt");

    dbg!(part1(input));
    dbg!(part2(input));
}

fn part1(input: &str) -> usize {
    input
        .lines()
        .map(|line| {
            let start = line.find(':').expect("No :");
            let [ref winning, ref have] = line[start + 1..]
                .split('|')
                .map(|numbers| {
                    numbers
                        .split(' ')
                        .filter(|v| !v.is_empty())
                        .map(|v| v.parse().unwrap())
                        .collect()
                })
                .collect::<Vec<HashSet<usize>>>()[..]
            else {
                unreachable!()
            };

            let winners: Vec<_> = winning.intersection(&have).collect();

            match winners.len() {
                0 => 0,
                1 => 1,
                v => 2usize.pow(v as u32) / 2,
            }
        })
        // .inspect(|card| {
        //     dbg!(card);
        // })
        .sum()
}

fn part2(input: &str) -> usize {
    let multipliers: Vec<_> = input
        .lines()
        .map(|line| {
            let start = line.find(':').expect("No :");
            let [ref winning, ref have] = line[start + 1..]
                .split('|')
                .map(|numbers| {
                    numbers
                        .split(' ')
                        .filter(|v| !v.is_empty())
                        .map(|v| v.parse().unwrap())
                        .collect()
                })
                .collect::<Vec<HashSet<usize>>>()[..]
            else {
                unreachable!()
            };

            winning.intersection(&have).count()
        })
        // .inspect(|card| {
        //     dbg!(card);
        // })
        .collect();

    let mut counts = vec![1; multipliers.len()];

    for (idx, multiplier) in multipliers.iter().enumerate() {
        let base = counts[idx];
        for count in counts[idx + 1..(idx + multiplier + 1)].iter_mut() {
            *count += base
        }
    }

    counts.iter().sum()
}
