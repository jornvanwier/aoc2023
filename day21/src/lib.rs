use std::collections::HashSet;
use std::mem;

fn parse_input(input: &str) -> (Vec<Vec<char>>, (usize, usize)) {
    let grid: Vec<Vec<_>> = input
        .lines()
        .map(str::chars)
        .map(Iterator::collect)
        .collect();

    let start = grid
        .iter()
        .enumerate()
        .filter_map(|(row, line)| {
            line.iter()
                .position(|&c| c == 'S')
                .map(|column| (row, column))
        })
        .next()
        .unwrap();

    debug_assert_eq!(grid[start.0][start.1], 'S');
    (grid, start)
}

pub fn part1(input: &str) -> usize {
    let (grid, start) = parse_input(input);

    let mut prev_positions = HashSet::new();
    let mut positions = HashSet::from([start]);

    const STEPS: usize = 64;
    for _ in 0..STEPS {
        mem::swap(&mut positions, &mut prev_positions);
        positions.clear();

        for &position in &prev_positions {
            for neighbor in [(-1, 0), (1, 0), (0, -1), (0, 1)]
                .iter()
                .filter_map(|&offset| {
                    let (pos_y, pos_x) = position;
                    let (offset_y, offset_x) = offset;

                    let neighbor_y = pos_y as isize + offset_y;
                    let neighbor_x = pos_x as isize + offset_x;

                    let y_max = grid.len();
                    let x_max = grid[0].len();

                    if neighbor_y >= 0 && neighbor_x >= 0 {
                        let neighbor_y = neighbor_y as usize;
                        let neighbor_x = neighbor_x as usize;

                        if neighbor_y < y_max && neighbor_x < x_max {
                            Some((neighbor_y, neighbor_x))
                        } else {
                            None
                        }
                    } else {
                        None
                    }
                })
            {
                if matches!(grid[neighbor.0][neighbor.1], '.' | 'S') {
                    positions.insert(neighbor);
                }
            }
        }
    }

    positions.len()
}

fn wrap_coord(value: isize, max: usize) -> usize {
    let max = max as isize;
    (if value < 0 {
        max + (value % max)
    } else {
        value
    } % max) as usize
}

pub fn part2(input: &str) -> usize {
    let (grid, start) = parse_input(input);

    // const STEPS: usize = 10;
    let grid_size = grid.len();

    dbg!(start.0);

    // This gives:
    //  a0 = 3955
    //  a1 = 35214
    //  a2 = 97607
    // let a0 = count_steps(&grid, start, start.0);
    // let a1 = count_steps(&grid, start, start.0 + grid_size);
    // let a2 = count_steps(&grid, start, start.0 + grid_size * 2);

    // Give the previous results to wolfram alpha:
    // https://www.wolframalpha.com/input?i=quadratic+function+%5B%2865%2C+3955%29%2C+%28196%2C+35214%29%2C+%28327%2C+97607%29%5D
    let f = |n: usize| {
        (((15567 * n.pow(2)) as f64 / 17161.) + (31942 * n) as f64 / 17161. + (24950f64 / 17161.))
            as usize
    };

    dbg!(f(1));
    dbg!(f(start.0 + grid_size));
    dbg!(f(26501365));

    f(26501365)
}

#[allow(unused)]
fn count_steps(grid: &[Vec<char>], start: (usize, usize), steps: usize) -> usize {
    let mut prev_positions = HashSet::new();
    // insert start
    let mut positions = HashSet::from([(start.0 as isize, start.1 as isize)]);

    for _ in 0..steps {
        mem::swap(&mut positions, &mut prev_positions);
        positions.clear();

        for &(pos_y, pos_x) in &prev_positions {
            for offset in [(-1, 0), (1, 0), (0, -1), (0, 1)] {
                let (offset_y, offset_x) = offset;

                let neighbor_y = pos_y + offset_y;
                let neighbor_x = pos_x + offset_x;

                let wrapped_y = wrap_coord(neighbor_y, grid.len());
                let wrapped_x = wrap_coord(neighbor_x, grid.len());

                if grid[wrapped_y][wrapped_x] != '#' {
                    positions.insert((neighbor_y, neighbor_x));
                }
            }
        }
    }

    positions.len()
}

#[cfg(test)]
mod tests {
    use crate::wrap_coord;

    #[test]
    fn test_wrap() {
        assert_eq!(wrap_coord(10, 11), 10);
        assert_eq!(wrap_coord(11, 11), 0);
        assert_eq!(wrap_coord(-1, 11), 10);
    }
}
