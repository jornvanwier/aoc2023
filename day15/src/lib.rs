pub fn part1(input: &str) -> usize {
    input.split(',').map(|step| hash(step)).sum()
}

fn hash(step: &str) -> usize {
    step.chars().fold(0, |acc, c| (acc + c as usize) * 17 % 256)
}

pub fn part2(input: &str) -> usize {
    let mut boxes = vec![Vec::with_capacity(10); 256];

    for step in input.split(',') {
        let mut parts = step.split(&['=', '-']);
        let label = parts.next().unwrap();
        let focal_length = parts.next().unwrap();

        let label_hash = hash(label);
        let current_box = &mut boxes[label_hash];

        match focal_length.is_empty() {
            true => {
                if let Some(idx) = current_box
                    .iter()
                    .position(|(stored_label, _)| *stored_label == label)
                {
                    current_box.remove(idx);
                }
            }
            false => {
                let focal_length: usize = focal_length.parse().unwrap();
                if let Some((_, value)) = current_box
                    .iter_mut()
                    .filter(|(stored_label, _)| *stored_label == label)
                    .next()
                {
                    *value = focal_length;
                } else {
                    current_box.push((label, focal_length))
                }
            }
        }
    }

    boxes
        .iter()
        .enumerate()
        .map(|(box_idx, current_box)| {
            current_box
                .iter()
                .enumerate()
                .map(|(slot_idx, (_, focal_length))| (box_idx + 1) * (slot_idx + 1) * *focal_length)
                .sum::<usize>()
        })
        .sum()
}
