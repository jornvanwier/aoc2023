use rayon::prelude::{IntoParallelIterator, ParallelIterator};
use std::collections::HashSet;
use std::ops::{Add, Range};

#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash)]
enum Direction {
    North,
    East,
    South,
    West,
}

impl Add<Direction> for (usize, usize) {
    type Output = (isize, isize);

    fn add(self, rhs: Direction) -> Self::Output {
        let (x, y) = self;
        match rhs {
            Direction::North => (x as isize, y as isize - 1),
            Direction::East => (x as isize + 1, y as isize),
            Direction::South => (x as isize, y as isize + 1),
            Direction::West => (x as isize - 1, y as isize),
        }
    }
}

fn check_bounds(input: &Vec<Vec<char>>, (x, y): (isize, isize)) -> Option<(usize, usize)> {
    if (0..input.len() as isize).contains(&y) && (0..input[0].len() as isize).contains(&x) {
        Some((x as usize, y as usize))
    } else {
        None
    }
}

macro_rules! checked_add_pos {
    ($input:ident, $position:ident, $direction:ident) => {
        if let Some(new_position) = check_bounds($input, $position + $direction) {
            $position = new_position;
        } else {
            return;
        }
    };
}

fn walk(
    input: &Vec<Vec<char>>,
    mut position: (usize, usize),
    mut direction: Direction,
    visited: &mut HashSet<((usize, usize), Direction)>,
    energized: &mut HashSet<(usize, usize)>,
) {
    loop {
        if !visited.insert((position, direction)) {
            // Back at visited tile, traveling in same direction
            return;
        }

        energized.insert(position);

        match input[position.1][position.0] {
            '.' => checked_add_pos!(input, position, direction),
            '/' => {
                direction = match direction {
                    Direction::North => Direction::East,
                    Direction::East => Direction::North,
                    Direction::South => Direction::West,
                    Direction::West => Direction::South,
                };

                checked_add_pos!(input, position, direction);
            }
            '\\' => {
                direction = match direction {
                    Direction::North => Direction::West,
                    Direction::East => Direction::South,
                    Direction::South => Direction::East,
                    Direction::West => Direction::North,
                };

                checked_add_pos!(input, position, direction);
            }
            '|' => match direction {
                Direction::North => checked_add_pos!(input, position, direction),
                Direction::South => checked_add_pos!(input, position, direction),
                Direction::East | Direction::West => {
                    // Recurse for one path, continue here for the other
                    if let Some(north_path) = check_bounds(input, position + Direction::North) {
                        walk(input, north_path, Direction::North, visited, energized);
                    }
                    direction = Direction::South;
                    checked_add_pos!(input, position, direction);
                }
            },
            '-' => match direction {
                Direction::North | Direction::South => {
                    // Recurse for one path, continue here for the other
                    if let Some(east_path) = check_bounds(input, position + Direction::East) {
                        walk(input, east_path, Direction::East, visited, energized);
                    }
                    direction = Direction::West;
                    checked_add_pos!(input, position, direction);
                }
                Direction::East => checked_add_pos!(input, position, direction),
                Direction::West => checked_add_pos!(input, position, direction),
            },

            _ => unreachable!(),
        }
    }
}

pub fn part1(input: &str) -> usize {
    let input: Vec<Vec<char>> = input.lines().map(|line| line.chars().collect()).collect();
    let start_position = (0, 0);
    let start_direction = Direction::East;

    count_energized(&input, start_position, start_direction)
}

fn count_energized(
    input: &Vec<Vec<char>>,
    start_position: (usize, usize),
    start_direction: Direction,
) -> usize {
    let mut energized = HashSet::new();
    walk(
        &input,
        start_position,
        start_direction,
        &mut HashSet::new(),
        &mut energized,
    );

    // println!("{start_direction:?} {start_position:?}: {}", energized.len());

    energized.len()
}

#[allow(unused)]
fn show_energized(input: &Vec<Vec<char>>, energized: &HashSet<(usize, usize)>) {
    for y in 0..input.len() {
        let line = &input[y];

        for x in 0..line.len() {
            if energized.contains(&(x, y)) {
                print!("#");
            } else {
                print!("{}", line[x]);
            }
        }
        println!()
    }
}

pub fn part2(input: &str) -> usize {
    let input: Vec<Vec<char>> = input.lines().map(|line| line.chars().collect()).collect();

    // Its probably square, but who knows
    let height = input.len();
    let width = input[0].len();

    let directions: [(
        Direction,
        Range<usize>,
        Box<dyn Fn(usize) -> (usize, usize) + Sync>,
    ); 4] = [
        (Direction::North, 0..width, Box::new(|x| (x, height - 1))),
        (Direction::East, 0..height, Box::new(|y| (0, y))),
        (Direction::South, 0..width, Box::new(|x| (x, 0))),
        (Direction::West, 0..height, Box::new(|y| (width - 1, y))),
    ];
    directions
        .into_par_iter()
        .flat_map(|(direction, range, pos_fn)| {
            range
                .clone()
                .into_par_iter()
                .map(|v| count_energized(&input, pos_fn(v), *direction))
                .max()
        })
        .max()
        .unwrap()
}
