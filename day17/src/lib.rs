use std::cmp::Ordering;
use std::collections::{BinaryHeap, HashMap};

type Position = (usize, usize);
type Node = (Position, (isize, isize));

fn check_coord(value: usize, offset: isize, max: usize) -> Option<usize> {
    if let Some(new_value) = value.checked_add_signed(offset) {
        if new_value < max {
            return Some(new_value);
        }
    }

    None
}

#[derive(Copy, Clone)]
struct State {
    g_score: usize,
    f_score: f64,
    position: Node,
}

impl State {
    fn cost(&self) -> f64 {
        self.g_score as f64 + self.f_score
    }
}

impl PartialEq for State {
    fn eq(&self, other: &Self) -> bool {
        self.g_score == other.g_score && self.position == other.position
    }
}

impl Eq for State {}

// The priority queue depends on `Ord`.
// Explicitly implement the trait so the queue becomes a min-heap
// instead of a max-heap.
impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        // Notice that the we flip the ordering on costs.
        // In case of a tie we compare positions - this step is necessary
        // to make implementations of `PartialEq` and `Ord` consistent.
        other
            .cost()
            .total_cmp(&self.cost())
            .then_with(|| self.position.cmp(&other.position))
    }
}

// `PartialOrd` needs to be implemented as well.
impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn a_star(
    grid: &Vec<Vec<usize>>,
    start: Position,
    goal: Position,
    min_straight: usize,
    max_straight: usize,
) -> Option<(Vec<Position>, usize)> {
    // let heuristic = |node: Position| {
    //     ((node.0 as f64 - goal.0 as f64).powf(2.) + (node.1 as f64 - goal.1 as f64).powf(2.)).sqrt()
    // };
    let heuristic = |node: Position| (node.0.abs_diff(goal.0) + node.1.abs_diff(goal.1)) as f64;

    let width = grid[0].len();
    let height = grid.len();

    let mut open_set = BinaryHeap::new();
    open_set.push(State {
        g_score: 0,
        f_score: 0.,
        position: (start, (0, 0)),
    });

    let mut came_from: HashMap<Node, Node> = HashMap::new();

    let mut g_score = HashMap::new();
    g_score.insert((start, (0, 0)), 0);

    while let Some(State {
        g_score: current_g_score,
        position: current,
        f_score: _,
    }) = open_set.pop()
    {
        // println!("Exploring {current:?}");

        if current.0 == goal {
            let mut path = vec![];
            let mut path_node = current;
            loop {
                path.push(path_node.0);
                if let Some(node) = came_from.get(&path_node) {
                    path_node = *node;
                } else {
                    break;
                }
            }
            return Some((path, current_g_score));
        }

        if current_g_score > g_score[&current] {
            continue;
        }

        let neighbor_offsets = [(1, 0), (-1, 0), (0, 1), (0, -1)];

        for neighbor_offset in neighbor_offsets {
            let neighbor = match (
                check_coord(current.0 .0, neighbor_offset.0, width),
                check_coord(current.0 .1, neighbor_offset.1, height),
            ) {
                (Some(neighbor_x), Some(neighbor_y)) => (neighbor_x, neighbor_y),
                _ => continue,
            };

            let same_direction = if let Some(previous) = came_from.get(&current) {
                if neighbor == previous.0 {
                    // No going back
                    continue;
                }

                neighbor_offset
                    == (
                        current.0 .0 as isize - previous.0 .0 as isize,
                        current.0 .1 as isize - previous.0 .1 as isize,
                    )
            } else {
                false
            };
            let neighbor_node = if same_direction {
                (
                    neighbor,
                    (
                        current.1 .0 + neighbor_offset.0,
                        current.1 .1 + neighbor_offset.1,
                    ),
                )
            } else {
                (neighbor, neighbor_offset)
            };

            let current_straight_line = (neighbor_node.1 .0 + neighbor_node.1 .1).abs() as usize;
            let tentative_g_score = if current_straight_line <= max_straight
                && (same_direction || (current.1 .0 + current.1 .1).abs() as usize >= min_straight)
                || came_from.get(&current).is_none()
            {
                current_g_score + grid[neighbor.1][neighbor.0]
            } else {
                usize::MAX
            };

            if tentative_g_score < *g_score.get(&neighbor_node).unwrap_or(&usize::MAX) {
                came_from.insert(neighbor_node, current);
                g_score.insert(neighbor_node, tentative_g_score);

                let next = State {
                    g_score: tentative_g_score,
                    f_score: heuristic(neighbor),
                    position: neighbor_node,
                };

                open_set.push(next)
            }
        }
    }

    None
}

fn parse_grid(input: &str) -> Vec<Vec<usize>> {
    let grid: Vec<Vec<usize>> = input
        .lines()
        .map(|line| {
            line.chars()
                .map(|c| c.to_digit(10).unwrap() as usize)
                .collect()
        })
        .collect();
    grid
}

#[allow(unused)]
fn print_path(grid: Vec<Vec<usize>>, path: Vec<Position>) {
    let mut draw: Vec<Vec<char>> = grid
        .iter()
        .map(|line| {
            line.iter()
                .map(|v| char::from_digit(*v as u32, 10).unwrap())
                .collect()
        })
        .collect();

    for (previous, current) in path.iter().rev().zip(path.iter().rev().skip(1)) {
        if let Some(replace) = match (
            current.0 as isize - previous.0 as isize,
            current.1 as isize - previous.1 as isize,
        ) {
            (1, 0) => Some('>'),
            (-1, 0) => Some('<'),
            (0, 1) => Some('v'),
            (0, -1) => Some('^'),
            _ => unreachable!(),
        } {
            draw[current.1][current.0] = replace;
        }
    }

    for y in 0..draw.len() {
        let line = &draw[y];
        for x in 0..line.len() {
            print!(
                "{}",
                if matches!(line[x], '>' | '<' | 'v' | '^') {
                    line[x]
                } else {
                    '.'
                }
            );
            // print!("{}", line[x]);
        }
        println!()
    }
}

pub fn part1(input: &str) -> usize {
    let grid = parse_grid(input);

    let (_path, score) = a_star(&grid, (0, 0), (grid[0].len() - 1, grid.len() - 1), 0, 3).unwrap();

    score
}

pub fn part2(input: &str) -> usize {
    let grid = parse_grid(input);

    let (_path, score) = a_star(&grid, (0, 0), (grid[0].len() - 1, grid.len() - 1), 4, 10).unwrap();

    score
}
