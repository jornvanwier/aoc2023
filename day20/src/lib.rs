use crate::ModuleType::{Broadcast, Conjunction, FlipFlop};
use itertools::Itertools;
use std::collections::{HashMap, VecDeque};
use std::hash::Hash;

#[derive(Debug)]
enum ModuleType<'input> {
    FlipFlop(bool),
    Conjunction { inputs: HashMap<&'input str, bool> },
    Broadcast,
}

#[derive(Debug)]
struct Module<'input> {
    module_type: ModuleType<'input>,
    outputs: Vec<&'input str>,
}

struct Signal<'input> {
    sender: &'input str,
    destination: &'input str,
    pulse: bool,
}

fn enqueue_pulse<'input>(
    queue: &mut VecDeque<Signal<'input>>,
    sender: &'input str,
    outputs: &[&'input str],
    pulse: bool,
) {
    queue.extend(
        outputs
            .iter()
            // .inspect(|&&output| {
            //     println!(
            //         "Processing pulse: {sender} -{}-> {output}",
            //         if pulse { "high" } else { "low" }
            //     );
            // })
            .map(|&output| Signal {
                sender,
                destination: output,
                pulse,
            }),
    );
}

fn run(modules: &mut HashMap<&str, Module>, suspects: &[&str], idx: usize) -> (usize, usize) {
    let mut pulses: VecDeque<Signal> = VecDeque::from([Signal {
        sender: "button",
        destination: "roadcaster",
        pulse: false,
    }]);

    let mut high = 0;
    let mut low = 0;

    while let Some(Signal {
        sender,
        destination,
        pulse,
    }) = pulses.pop_front()
    {
        *(if pulse { &mut high } else { &mut low }) += 1;

        if let Some(module) = modules.get_mut(destination) {
            if let Some(new_pulse) = match &mut module.module_type {
                FlipFlop(state) => {
                    if !pulse {
                        *state = !*state;

                        Some(*state)
                    } else {
                        None
                    }
                }
                Conjunction { inputs } => {
                    inputs.insert(sender, pulse);

                    let state = !inputs.values().all(|v| *v);

                    if suspects.contains(&destination) && state {
                        println!("leggo {destination} {idx}");
                    }

                    Some(state)
                }
                Broadcast => Some(pulse),
            } {
                enqueue_pulse(&mut pulses, destination, &module.outputs, new_pulse)
            }
        }
    }

    (high, low)
}

fn parse_input(input: &str) -> HashMap<&str, Module> {
    let mut modules: HashMap<_, _> = input
        .lines()
        .map(|line| {
            let [description, outputs] = line.split(" -> ").collect::<Vec<_>>()[..] else {
                unreachable!()
            };

            let (type_ident, name) = description.split_at(1);

            let module_type = match type_ident {
                "%" => FlipFlop(false),
                "&" => Conjunction {
                    inputs: HashMap::new(),
                },
                _ => {
                    debug_assert_eq!(name, "roadcaster");
                    Broadcast
                }
            };

            let outputs = outputs.split(", ").collect();

            (
                name,
                Module {
                    module_type,
                    outputs,
                },
            )
        })
        .collect();

    // initialize conjunction modules
    let connections: Vec<_> = modules
        .iter()
        .flat_map(|(&key, value)| value.outputs.iter().map(move |&output| (key, output)))
        .collect();
    for (from, to) in connections {
        if to != "output" {
            if let Some(module) = modules.get_mut(to) {
                if let Conjunction { inputs } = &mut module.module_type {
                    inputs.insert(from, false);
                }
            } else {
                eprintln!("Unknown module {to}");
            }
        }
    }
    modules
}

pub fn part1(input: &str) -> usize {
    let mut modules = parse_input(input);

    let mut high_pulse = 0;
    let mut low_pulse = 0;

    for idx in 0..1_000 {
        let (high, low) = run(&mut modules, &[], idx);
        high_pulse += high;
        low_pulse += low;
    }

    high_pulse * low_pulse
}

pub fn part2(input: &str) -> usize {
    let mut modules = parse_input(input);

    dbg!(modules
        .iter()
        .find(|(_, module)| module.outputs.contains(&"rx"))
        .iter()
        .collect_vec());

    let (&rx_input_name, _) = modules
        .iter()
        .find(|(_, module)| module.outputs.contains(&"rx"))
        .unwrap();

    fn get_inputs<'input, 'modules>(
        name: &'input str,
        modules: &'modules HashMap<&'input str, Module<'input>>,
    ) -> &'modules HashMap<&'input str, bool> {
        if let Conjunction { inputs } = &modules[name].module_type {
            inputs
        } else {
            panic!("Unexpected input to rx")
        }
    }

    let suspects: Vec<_> = get_inputs(rx_input_name, &modules)
        .keys()
        .cloned()
        .collect();

    dbg!(&suspects);

    // Kill program, after all suspects have completed 1 cycle, fill in here.
    dbg!([3916, 3942, 3946, 4000,].iter().fold(1, |a, b| lcm(a, *b + 1)));

    for idx in 1.. {
        run(&mut modules, &suspects, idx);
    }

    0
}

fn gcd(a: usize, b: usize) -> usize {
    match ((a, b), (a & 1, b & 1)) {
        ((x, y), _) if x == y => y,
        ((0, x), _) | ((x, 0), _) => x,
        ((x, y), (0, 1)) | ((y, x), (1, 0)) => gcd(x >> 1, y),
        ((x, y), (0, 0)) => gcd(x >> 1, y >> 1) << 1,
        ((x, y), (1, 1)) => {
            let (x, y) = (x.min(y), x.max(y));
            gcd((y - x) >> 1, x)
        }
        _ => unreachable!(),
    }
}

fn lcm(a: usize, b: usize) -> usize {
    a * b / gcd(a, b)
}
