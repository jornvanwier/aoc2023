use std::collections::{HashMap, HashSet, VecDeque};
use std::num::ParseIntError;
use std::ops::RangeInclusive;
use rayon::prelude::*;

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
struct Coord {
    x: usize,
    y: usize,
    z: usize,
}

impl TryFrom<&str> for Coord {
    type Error = String;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let parts: Vec<_> = value
            .split(',')
            .map(|value| value.parse().map_err(|e: ParseIntError| e.to_string()))
            .collect::<Result<_, _>>()?;

        let [x, y, z] = parts[..] else {
            return Err("Invalid format".to_string());
        };

        Ok(Self { x, y, z })
    }
}

#[derive(Eq, PartialEq, Hash, Debug)]
struct Brick(Coord, Coord, String);

impl Brick {
    fn xs(&self) -> RangeInclusive<usize> {
        let xs = self.0.x..=self.1.x;
        debug_assert!(xs.clone().count() >= 1);
        xs
    }

    fn ys(&self) -> RangeInclusive<usize> {
        let ys = self.0.y..=self.1.y;
        debug_assert!(ys.clone().count() >= 1);
        ys
    }
    fn ray_up<'brick>(&self, others: &'brick [Brick]) -> (usize, Vec<&'brick Brick>) {
        let top = self.1.z;
        debug_assert!(top >= self.0.z);

        // Only check directly on top?
        let max = top + 1;

        self.ray(others, top + 1..=max, |z, Brick(from, ..)| from.z == z)
    }

    fn ray_down<'brick>(&self, others: &'brick [Brick]) -> (usize, Vec<&'brick Brick>) {
        let bottom = self.0.z;
        debug_assert!(bottom <= self.1.z);

        self.ray(others, (1..bottom).rev(), |z, Brick(_from, to, ..)| {
            to.z == z
        })
    }

    fn ray<'brick, I: Iterator<Item = usize>, FZFilter: Fn(usize, &Brick) -> bool>(
        &self,
        others: &'brick [Brick],
        zs: I,
        z_filter: FZFilter,
    ) -> (usize, Vec<&'brick Brick>) {
        let mut hits = vec![];
        for z in zs {
            for candidate in others.iter().filter(|brick| z_filter(z, brick)) {
                if range_overlap(self.xs(), candidate.xs())
                    && range_overlap(self.ys(), candidate.ys())
                {
                    // println!(
                    //     "Overlap:\n\t{:?} / {:?}\n\t{:?} / {:?}",
                    //     self.xs(),
                    //     candidate.xs(),
                    //     self.ys(),
                    //     candidate.ys()
                    // );
                    hits.push(candidate)
                }
            }

            if hits.len() > 0 {
                return (z, hits);
            }
        }

        debug_assert!(hits.is_empty());
        (0, hits)
    }
}

fn range_overlap(a: RangeInclusive<usize>, b: RangeInclusive<usize>) -> bool {
    for value in a {
        if b.contains(&value) {
            return true;
        }
    }
    return false;
    // a.start() <= b.end() && a.end() <= b.start()
    // a.start().max(b.start()) <= a.end().min(b.end())
}

fn parse_input(input: &str) -> Vec<Brick> {
    let mut bricks: Vec<_> = input
        .lines()
        .enumerate()
        .map(|(mut idx, line)| {
            let [from, to] = line
                .split('~')
                .map(Coord::try_from)
                .map(Result::unwrap)
                .collect::<Vec<Coord>>()[..]
            else {
                unreachable!()
            };

            debug_assert!(from.x <= to.x);
            debug_assert!(from.y <= to.y);
            debug_assert!(from.z <= to.z);

            let mut label = vec![];
            while idx >= 26 {
                label.push(('A' as u8 + ((idx % 26) as u8)) as char);
                idx /= 26;
            }
            label.push(('A' as u8 + ((idx % 26) as u8)) as char);

            Brick(from, to, label.iter().collect())
        })
        .collect();

    bricks.sort_by_key(|Brick(from, ..)| from.z);

    let mut idx = 0;

    while idx < bricks.len() {
        let (support_z, _) = bricks[idx].ray_down(&bricks);
        let brick = &mut bricks[idx];

        let fall_to = support_z + 1;
        let current_z = brick.0.z;

        let delta = current_z - fall_to;
        // println!("{} falling {delta} ({current_z} to {fall_to})", brick.2);

        brick.0.z -= delta;
        brick.1.z -= delta;

        idx += 1;
    }

    bricks
}

fn calculate_supports(bricks: &[Brick]) -> HashMap<&Brick, Vec<&Brick>> {
    let mut supports = HashMap::new();
    for brick in bricks {
        let (_y, bricks_on_top) = brick.ray_up(&bricks);

        for &other in &bricks_on_top {
            supports.entry(other).or_insert(vec![]).push(brick);
        }
    }

    // for (below, on_top) in &supports {
    //     print!("{} is supported by ", below.2);
    //     for other in on_top {
    //         print!("{}, ", other.2);
    //     }
    //     println!()
    // }

    supports
}

fn count_cascade(
    disintegrate: &Brick,
    supports: &HashMap<&Brick, Vec<&Brick>>,
    supports_rev: &HashMap<&Brick, Vec<&Brick>>,
) -> usize {
    let mut queue = VecDeque::from([disintegrate]);

    let mut fall = HashSet::new();

    while let Some(disintegrate) = queue.pop_front() {
        if let Some(candidates) = supports_rev.get(disintegrate) {
            for &candidate in candidates {
                if supports[candidate]
                    .iter()
                    .filter(|&&supporting| !fall.contains(supporting) && supporting != disintegrate)
                    .count()
                    == 0
                {
                    fall.insert(candidate);
                    queue.push_back(candidate);
                }
            }
        }
    }

    fall.len()
}

pub fn part1(input: &str) -> usize {
    let bricks = parse_input(input);
    let supports = calculate_supports(&bricks);

    let sole_supports: HashSet<_> = supports.values().filter(|above| above.len() == 1).collect();

    bricks.len() - sole_supports.len()
}

pub fn part2(input: &str) -> usize {
    let bricks = parse_input(input);
    let supports = calculate_supports(&bricks);

    let mut supports_rev = HashMap::with_capacity(supports.len());
    for (&brick, above) in &supports {
        for &other in above {
            supports_rev.entry(other).or_insert(vec![]).push(brick);
        }
    }

    bricks
        .par_iter()
        .map(|brick| {
            let count = count_cascade(brick, &supports, &supports_rev);
            // println!("{} falls {count}", brick.2);
            count
        })
        .sum()
}
