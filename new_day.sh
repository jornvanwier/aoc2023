#!/usr/bin/env bash

set -euo pipefail

last_day="$(echo day* | tr " " "\n" | awk '{ print substr($0, 4, length($0)) }' | sort -h | tail -n 1)"

next="day$(( $last_day + 1 ))"
echo "Adding $next"

cargo generate --path template --name "$next"
