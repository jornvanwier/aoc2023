fn main() {
    dbg!(part1(include_str!("../example.txt")));
    dbg!(part1(include_str!("../input.txt")));
    dbg!(part2(include_str!("../example.txt")));
    dbg!(part2(include_str!("../input.txt")));
}

fn find_gaps<I: Iterator<Item = usize> + Clone>(iterator: I) -> Vec<(usize, usize)> {
    iterator
        .clone()
        .zip(iterator.skip(1))
        .enumerate()
        .map(|(idx, (a, b))| (idx, b - a))
        .filter(|(_, delta)| delta >= &2)
        .collect()
}

fn part1(input: &str) -> usize {
    calculate_expanded_distance(input, 2)
}

fn calculate_expanded_distance(input: &str, expansion: usize) -> usize {
    let mut coords: Vec<_> = input
        .lines()
        .enumerate()
        .flat_map(|(line_idx, line)| {
            line.chars()
                .enumerate()
                .filter(|(_, c)| c == &'#')
                .map(move |(row_idx, _)| (line_idx, row_idx))
        })
        .collect();

    coords.sort_by(|a, b| a.0.cmp(&b.0));
    let y_gaps = find_gaps(coords.iter().map(|(y, _)| *y));

    for (y_idx, gap) in y_gaps {
        coords[y_idx + 1..].iter_mut().for_each(|(y, _)| {
            let expand = (gap - 1) * (expansion - 1);
            *y += expand
        });
    }

    coords.sort_by(|a, b| a.1.cmp(&b.1));
    let x_gaps = find_gaps(coords.iter().map(|(_, x)| *x));

    for (x_idx, gap) in x_gaps {
        coords[x_idx + 1..].iter_mut().for_each(|(_, x)| {
            let expand = (gap - 1) * (expansion - 1);
            *x += expand
        });
    }

    // let width = coords.iter().map(|(_, x)| x).max().unwrap();
    // let height = coords.iter().map(|(y, _)| y).max().unwrap();
    //
    // for y in 0..=*height {
    //     for x in 0..=*width {
    //         print!("{}", if coords.contains(&(y, x)) { '#' } else { '.' });
    //     }
    //     println!();
    // }

    coords
        .iter()
        .enumerate()
        .flat_map(|(idx, (a_y, a_x))| {
            coords[idx + 1..].iter().map(|(b_y, b_x)| {
                ((*a_x as isize - *b_x as isize).abs() + (*a_y as isize - *b_y as isize).abs())
                    as usize
            })
        })
        .sum()
}

fn part2(input: &str) -> usize {
    calculate_expanded_distance(input, 1_000_000)
}
