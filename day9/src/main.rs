fn main() {
    let input = include_str!("../input.txt");
    // let input = include_str!("../example.txt");

    dbg!(part1(input));
    dbg!(part2(input));
}

fn continue_sequence(numbers: &[i64]) -> Option<i64> {
    let diff: Vec<_> = numbers
        .iter()
        .zip(numbers.iter().skip(1))
        .map(|(number, next)| next - number)
        .collect();

    if diff.iter().all(|v| *v == 0) {
        None
    } else {
        match continue_sequence(&diff) {
            Some(increase) => Some(diff.last().unwrap() + increase),
            None => Some(*(diff.last().unwrap())),
        }
    }
}

fn continue_sequence_reverse(numbers: &[i64]) -> Option<i64> {
    let diff: Vec<_> = numbers
        .iter()
        .zip(numbers.iter().skip(1))
        .map(|(number, next)| next - number)
        .collect();

    if diff.iter().all(|v| *v == 0) {
        None
    } else {
        match continue_sequence_reverse(&diff) {
            Some(decrease) => Some(diff.first().unwrap() - decrease),
            None => Some(*(diff.first().unwrap())),
        }
    }
}

fn part1(input: &str) -> usize {
    input
        .lines()
        .map(|line| {
            let numbers: Vec<_> = line
                .split_whitespace()
                .map(|v| v.parse().unwrap())
                .collect();
            numbers.last().unwrap() + continue_sequence(&numbers).unwrap()
        })
        .sum::<i64>() as usize
}

fn part2(input: &str) -> usize {
    input
        .lines()
        .map(|line| {
            let numbers: Vec<_> = line
                .split_whitespace()
                .map(|v| v.parse().unwrap())
                .collect();
            numbers.first().unwrap() - continue_sequence_reverse(&numbers).unwrap()
        })
        .sum::<i64>() as usize
}
