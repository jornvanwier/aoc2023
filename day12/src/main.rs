use cached::proc_macro::cached;
use indicatif::ParallelProgressIterator;
use rayon::prelude::{IntoParallelIterator, ParallelIterator};

fn main() {
    dbg!(part1(include_str!("../example.txt")));
    dbg!(part1(include_str!("../input.txt")));
    dbg!(part2(include_str!("../example.txt")));
    dbg!(part2(include_str!("../input.txt")));
}

#[derive(Eq, PartialEq, Debug, Copy, Clone, Hash)]
enum Condition {
    Operational,
    Damaged,
}

fn parse_line(line: &str) -> (Vec<Option<Condition>>, Vec<usize>) {
    let mut parts = line.split_whitespace();
    let data = parts.next().unwrap();
    let spec = parts.next().unwrap();

    let spec: Vec<_> = spec
        .split(',')
        .map(|v| v.parse::<usize>().unwrap())
        .collect();

    let data: Vec<Option<Condition>> = data
        .chars()
        .map(|c| match c {
            '.' => Some(Condition::Operational),
            '#' => Some(Condition::Damaged),
            '?' => None,
            _ => unreachable!(),
        })
        .collect();
    (data, spec)
}

#[allow(unused)]
fn show_solution(values: &[Option<Condition>]) -> String {
    values
        .iter()
        .map(|condition| match condition {
            None => '?',
            Some(Condition::Operational) => '.',
            Some(Condition::Damaged) => '#',
        })
        .collect()
}

#[cached(
    key="String",
    convert=r#"{ format!("{}, {spec:?}", show_solution(data)) }"#
)]
fn solve(data: &[Option<Condition>], spec: &[usize]) -> usize {
    let [spec_first, spec_rest @ ..] = spec else {
        // println!("ACCEPT {}", line!());
        return 1;
    };

    if data.len() < spec.iter().sum() {
        // println!("REJECT {}", line!());
        return 0;
    }

    // if data.len() < *spec_first {
    //     return 0;
    // }

    // println!("{solution} {spec:?}", solution = show_solution(data));

    match &data[..*spec_first] {
        [Some(Condition::Operational), ..] => solve(&data[1..], spec),
        [Some(Condition::Damaged) | None, rest @ ..]
            if rest
                .iter()
                .all(|v| matches!(v, Some(Condition::Damaged) | None)) =>
        {
            let end_of_data = data.len() <= *spec_first + 1;
            let sequence_ends =
                end_of_data || matches!(data[*spec_first], Some(Condition::Operational) | None);

            (if sequence_ends {
                if spec_rest.is_empty() {
                    if data[*spec_first..].iter().any(|c| c == &Some(Condition::Damaged)) {
                        // println!("REJECT {}", line!());
                        0
                    } else {
                        // println!("ACCEPT {}", line!());
                        1
                    }
                } else {
                    solve(&data[(spec_first + 1)..], spec_rest)
                }
            } else {
                // println!("REJECT {}", line!());
                0
            }) + (if data[0] == None {
                solve(&data[1..], spec)
            } else {
                // println!("REJECT {}", line!());
                0
            })
        }
        [None, ..] => solve(&data[1..], spec),
        _ => 0,
    }
}

fn part1(input: &str) -> usize {
    input
        .lines()
        .filter(|line| !line.starts_with('/'))
        .map(|line| {
            let (data, spec) = parse_line(line);

            solve(&data, &spec)
        })
        // .zip(input.lines())
        // .inspect(|(arrangements, line)| println!("{line} - {arrangements}"))
        // .map(|(arrangements, _)| arrangements)
        .sum()
}

fn part2(input: &str) -> usize {
    let parsed: Vec<_> = input
        .lines()
        .filter(|line| !line.starts_with('/'))
        .map(|line| {
            let (data, spec) = parse_line(line);

            let orig_length = data.len();
            let mut data = data.repeat(5);

            for idx in 1..5 {
                data.insert(orig_length * idx + idx - 1, None);
            }

            // dbg!(show_solution(&data));

            let spec = spec.repeat(5);

            (data, spec)
        })
        .collect();

    parsed
        .into_par_iter()
        .map(|(data, spec): (_, Vec<_>)| solve(&data, &spec))
        // .zip(input.lines())
        // .inspect(|(value, line)| println!("{line} - {value}"))
        // .map(|(value, _)| value)
        // .progress()
        .sum()
}


#[cfg(test)]
mod tests {
    use crate::{parse_line, solve};

    #[test]
    fn test_1() {
        let (data, spec) = parse_line("????#.?.?#?? 3,1");
        assert_eq!(solve(&data, &spec), 1);
    }

    #[test]
    fn test_2() {
        let (data, spec) = parse_line("#???#???#?????.#.#? 12,1,2");
        assert_eq!(solve(&data, &spec), 1);
    }
}
