pub fn part1(input: &str) -> usize {
    input
        .split("\n\n")
        .map(|pattern| {
            let lines: Vec<_> = pattern.lines().collect();

            if let Some(horizontal) = find_symmetry(&lines, 0) {
                // println!("Horizontal symmetry at row={horizontal}");
                horizontal * 100
            } else if let Some(vertical) = find_symmetry(&transpose(&lines), 0) {
                // println!("Vertical symmetry at column={vertical}");
                vertical
            } else {
                panic!("No symmetry")
            }
        })
        .sum()
}

trait CountFlaws {
    fn flaws(&self, other: &Self) -> usize;
}
impl<S: AsRef<str>> CountFlaws for S {
    fn flaws(&self, other: &Self) -> usize {
        self.as_ref().chars()
            .zip(other.as_ref().chars())
            .filter(|(a, b)| a != b)
            .count()
    }
}

fn count_flaws<T: CountFlaws, I: IntoIterator<Item = T>>(left: I, right: I) -> usize
where
    <I as IntoIterator>::IntoIter: DoubleEndedIterator,
{
    left.into_iter()
        .rev()
        .zip(right.into_iter())
        .map(|(a, b)| a.flaws(&b))
        .sum()
}

fn find_symmetry<S: AsRef<str>>(lines: &[S], threshold: usize) -> Option<usize> {
    for split_point in 1..lines.len() {
        let (left, right) = lines.split_at(split_point);

        if count_flaws(left, right) == threshold {
            return Some(split_point);
        }
    }
    None
}

fn transpose(v: &[&str]) -> Vec<String> {
    assert!(!v.is_empty());
    let len = v[0].len();
    let mut iters: Vec<_> = v.into_iter().map(|n| n.chars()).collect();
    (0..len)
        .map(|_| {
            iters
                .iter_mut()
                .map(|n| n.next().unwrap())
                .collect::<String>()
        })
        .collect()
}

pub fn part2(input: &str) -> usize {
    input
        .split("\n\n")
        .map(|pattern| {
            let lines: Vec<_> = pattern.lines().collect();

            if let Some(horizontal) = find_symmetry(&lines, 1) {
                horizontal * 100
            } else if let Some(vertical) = find_symmetry(&transpose(&lines), 1) {
                vertical
            } else {
                panic!("No symmetry")
            }
        })
        .sum()
}
