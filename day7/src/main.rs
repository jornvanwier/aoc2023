mod part1;
mod part2;

fn main() {
    let example = include_str!("../example.txt");
    let input = include_str!("../input.txt");

    dbg!(part1::part1(example));
    dbg!(part1::part1(input));

    dbg!(part2::part2(example));
    dbg!(part2::part2(input));
}
