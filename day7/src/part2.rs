use std::cmp::Ordering;
use std::collections::HashMap;
use std::fmt::{Debug, Display, Formatter};

#[derive(Eq, PartialEq, Ord, PartialOrd, Hash, Copy, Clone, Debug)]
enum Card {
    J,
    Digit(u32),
    T,
    Q,
    K,
    A,
}

impl TryFrom<char> for Card {
    type Error = String;

    fn try_from(value: char) -> Result<Card, Self::Error> {
        match value {
            'A' => Ok(Card::A),
            'K' => Ok(Card::K),
            'Q' => Ok(Card::Q),
            'J' => Ok(Card::J),
            'T' => Ok(Card::T),
            '2'..='9' => Ok(Card::Digit(value.to_digit(10).unwrap())),
            _ => Err("Invalid".into()),
        }
    }
}

impl Display for Card {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Card::Digit(v) => v.to_string(),
                Card::T => 'T'.to_string(),
                Card::J => 'J'.to_string(),
                Card::Q => 'Q'.to_string(),
                Card::K => 'K'.to_string(),
                Card::A => 'A'.to_string(),
            }
        )
    }
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Copy, Clone, Debug)]
enum HandType {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

#[derive(Eq, PartialEq)]
struct Hand([Card; 5]);

impl Hand {
    fn count_uniques(&self) -> HashMap<Card, usize> {
        let mut uniques: HashMap<Card, usize> = HashMap::new();

        for card in self.0 {
            *uniques.entry(card).or_insert(0) += 1;
        }

        uniques
    }

    fn hand_type(&self) -> HandType {
        let mut uniques = self.count_uniques();

        if uniques.len() == 1 {
            return FiveOfAKind;
        }

        let jokers = uniques
            .remove_entry(&Card::J)
            .map(|(_, count)| count)
            .unwrap_or(0);

        let mut uniques: Vec<_> = uniques.values().cloned().collect();
        uniques.sort_by(|a, b| a.cmp(b).reverse());

        uniques[0] += jokers;

        use HandType::*;

        let hand_type = if uniques.len() == 1 {
            FiveOfAKind
        } else {
            match uniques[..2] {
                [4, _] => FourOfAKind,
                [3, 2] => FullHouse,
                [3, _] => ThreeOfAKind,
                [2, 2] => TwoPair,
                [2, _] => OnePair,
                _ => HighCard,
            }
        };

        hand_type
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        let self_hand_type = self.hand_type();
        let other_hand_type = other.hand_type();

        if self_hand_type == other_hand_type {
            self.0
                .iter()
                .zip(&other.0)
                .map(|(a, b)| a.cmp(b))
                .filter(|v| *v != Ordering::Equal)
                .next()
                .unwrap_or(Ordering::Equal)
        } else {
            self_hand_type.cmp(&other_hand_type)
        }
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl TryFrom<&str> for Hand {
    type Error = String;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let values = value
            .chars()
            .map(Card::try_from)
            .collect::<Result<Vec<_>, String>>()?;

        Ok(Hand(values.try_into().map_err(|_| "oof")?))
    }
}

impl Display for Hand {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for card in self.0 {
            write!(f, "{}", card)?;
        }

        Ok(())
    }
}

impl Debug for Hand {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}

pub fn part2(input: &str) -> usize {
    let mut set: Vec<_> = input
        .lines()
        .map(|line| {
            let mut parts = line.split_whitespace();
            let hand_str = parts.next().unwrap();
            let hand: Hand = hand_str.try_into().unwrap();

            let bid: usize = parts.next().unwrap().parse().unwrap();

            (hand, bid)
        })
        .collect();

    set.sort_by(|(a, _), (b, _)| a.cmp(b));

    set.iter()
        .enumerate()
        .map(|(rank, (_, bid))| (rank + 1) * bid)
        .sum()
}
