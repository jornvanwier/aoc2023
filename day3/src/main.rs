use std::collections::HashSet;

fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../input.txt");
    // let input = include_str!("../ruurd.txt");

    dbg!(part1(input));
    dbg!(part2(input));
}

struct Number {
    value: usize,
    line: usize,
    start: usize,
    end: usize,
}

fn part1(input: &str) -> usize {
    let mut symbol_positions = vec![];
    let mut symbols = HashSet::new();
    let mut numbers = vec![];

    for (line_idx, line) in input.lines().enumerate() {
        let mut column_idx = 0;

        let mut number_start = None;
        let characters: Vec<_> = line.chars().collect();

        while column_idx < line.len() {
            let character = characters[column_idx];
            if matches!(character, '0'..='9') {
                if number_start.is_none() {
                    number_start = Some(column_idx);
                }
            } else {
                close_number(&mut numbers, line_idx, &line, column_idx, number_start);

                number_start = None;

                if character != '.' {
                    symbol_positions.push((line_idx, column_idx));
                    symbols.insert(character);
                }
            }

            column_idx += 1;
        }

        close_number(&mut numbers, line_idx, &line, column_idx, number_start);
    }

    let mut valid_numbers = vec![];

    for number in numbers {
        'search: for line_idx in number.line.saturating_sub(1)..=number.line + 1 {
            for column_idx in number.start.saturating_sub(1)..=number.end + 1 {
                if symbol_positions.contains(&(line_idx, column_idx)) {
                    valid_numbers.push(number.value);
                    break 'search;
                }
            }
        }
    }

    dbg!(&valid_numbers);
    dbg!(&symbols);
    valid_numbers.iter().sum()
}

fn part2(input: &str) -> usize {
    let mut gear_positions = vec![];
    let mut numbers = vec![];

    for (line_idx, line) in input.lines().enumerate() {
        let mut column_idx = 0;

        let mut number_start = None;
        let characters: Vec<_> = line.chars().collect();

        while column_idx < line.len() {
            let character = characters[column_idx];
            if matches!(character, '0'..='9') {
                if number_start.is_none() {
                    number_start = Some(column_idx);
                }
            } else {
                close_number(&mut numbers, line_idx, &line, column_idx, number_start);

                number_start = None;

                if character == '*' {
                    gear_positions.push((line_idx, column_idx));
                }
            }

            column_idx += 1;
        }

        close_number(&mut numbers, line_idx, &line, column_idx, number_start);
    }

    gear_positions
        .iter()
        .filter_map(|(gear_line, gear_column)| {
            let numbers_in_range: Vec<_> = numbers
                .iter()
                .filter(|number| {
                    (number.line.saturating_sub(1)..=number.line + 1).contains(&gear_line)
                        && (number.start.saturating_sub(1)..=number.end + 1).contains(&gear_column)
                })
                .map(|number| number.value)
                .collect();

            if numbers_in_range.len() == 2 {
                Some(numbers_in_range.iter().fold(1, |a, b| a * b))
            } else {
                None
            }
        })
        .sum()
}

fn close_number(
    numbers: &mut Vec<Number>,
    line_idx: usize,
    line: &&str,
    column_idx: usize,
    number_start: Option<usize>,
) {
    if let Some(start) = number_start {
        let end = column_idx - 1;
        let value_str = &line[start..=end];
        numbers.push(Number {
            value: value_str.parse().unwrap(),
            line: line_idx,
            start,
            end,
        });
    }
}
